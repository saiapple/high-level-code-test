import PageContentRow from './PageContentRow';

export default class PageContentSection {
  private pageContentRow: Array<PageContentRow> = []

  get rows(): Array<PageContentRow> {
    return this.pageContentRow;
  }

  set rows(rows: Array<PageContentRow>) {
    this.pageContentRow = rows;
  }

  createRowWithColumns(columnCount: number): void {
    if (columnCount > 6) {
      return;
    }

    this.rows.push(new PageContentRow(columnCount));
  }

  toJson() {
    const res: any = {
      rows: [],
    };

    this.rows.forEach((row: PageContentRow) => {
      res.rows.push(row.toJson());
    });

    return res;
  }

  initFromJson(jsonData: any) {
    jsonData.rows.forEach((row: any) => {
      this.createRowWithColumns(row.columns.length);
      this.rows[this.rows.length - 1].initFromJson(row);
    });
  }
}
