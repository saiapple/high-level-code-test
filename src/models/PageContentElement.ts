import Headline from './contents/Headline';
import List from './contents/List';
import Image from './contents/Image';
import Paragraph from './contents/Paragraph';

export default class PageContentElement {
  private element: Headline | List | Image | Paragraph = new Headline();

  constructor(elementType: string) {
    switch (elementType) {
      case 'image':
        this.element = new Image();
        break;
      case 'list':
        this.element = new List();
        break;
      case 'paragraph':
        this.element = new Paragraph();
        break;
      default:
        this.element = new Headline();
        break;
    }
  }

  get content(): Headline | List | Image | Paragraph {
    return this.element;
  }

  set content(element: Headline | List | Image | Paragraph) {
    this.element = element;
  }

  toJson() {
    const res: any = {};
    res.type = this.content.type;

    if (this.content instanceof List) {
      res.list = this.content.items;
    }

    if (this.content instanceof Image) {
      res.url = this.content.url;
      res.desc = this.content.desc;
    }

    if (this.content instanceof Headline) {
      res.text = this.content.text;
    }

    if (this.content instanceof Paragraph) {
      res.text = this.content.text;
    }

    return res;
  }

  initFromJson(jsonData: any) {
    if (this.content instanceof Image) {
      this.content.url = jsonData.url;
      this.content.desc = jsonData.desc;
    }

    if (this.content instanceof List) {
      this.content.items = jsonData.list;
    }

    if (this.content instanceof Headline) {
      this.content.text = jsonData.text;
    }

    if (this.content instanceof Paragraph) {
      this.content.text = jsonData.text;
    }
  }
}
