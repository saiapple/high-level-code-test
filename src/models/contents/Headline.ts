export default class Headline {
  private value = 'Heading Element';

  get type(): string {
    return 'headline';
  }

  get text(): string {
    return this.value;
  }

  set text(value: string) {
    this.value = value;
  }
}
