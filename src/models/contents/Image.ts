export default class Image {
  private imageUrl = 'https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg';

  private imageDescription = 'Baby yoda';

  get type(): string {
    return 'image';
  }

  get url(): string {
    return this.imageUrl;
  }

  set url(url: string) {
    this.imageUrl = url;
  }

  get desc(): string {
    return this.imageDescription;
  }

  set desc(desc: string) {
    this.imageDescription = desc;
  }
}
