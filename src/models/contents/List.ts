export default class List {
  private value: Array<string> = [
    'List 1',
    'List 2',
    'List 3',
  ];

  get type(): string {
    return 'list';
  }

  get items(): Array<string> {
    return this.value;
  }

  set items(items: Array<string>) {
    this.value = items;
  }
}
