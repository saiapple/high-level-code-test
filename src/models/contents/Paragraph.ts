export default class Paragraph {
  private value = 'Paragraph Element';

  get type(): string {
    return 'paragraph';
  }

  get text(): string {
    return this.value;
  }

  set text(value: string) {
    this.value = value;
  }
}
