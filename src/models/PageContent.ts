import PageContentSection from './PageContentSection';

export default class PageContent {
  private pageContentSections: Array<PageContentSection> = []

  get sections(): Array<PageContentSection> {
    return this.pageContentSections;
  }

  set sections(rows: Array<PageContentSection>) {
    this.pageContentSections = rows;
  }

  toJson() {
    const res: any = {
      sections: [],
    };

    this.sections.forEach((section: PageContentSection) => {
      res.sections.push(section.toJson());
    });

    return res;
  }

  initFromJson(jsonData: any) {
    jsonData.sections.forEach((section: any, index: number) => {
      this.sections.push(new PageContentSection());
      this.sections[index].initFromJson(section);
    });
  }
}
