import PageContentColumn from './PageContentColumn';
import PageContentElement from './PageContentElement';

export default class PageContentRow {
  private pageContentColumns: Array<PageContentColumn> = [];

  constructor(columnCount?: number) {
    if (columnCount !== undefined && columnCount > 0) {
      for (let i = 0; i < columnCount; i += 1) {
        this.columns.push(new PageContentColumn());
      }
    }
  }

  get columns(): Array<PageContentColumn> {
    return this.pageContentColumns;
  }

  set columns(columns: Array<PageContentColumn>) {
    this.pageContentColumns = columns;
  }

  toJson() {
    const res: any = {
      columns: [],
    };

    this.columns.forEach((column: PageContentColumn) => {
      res.columns.push(column.toJson());
    });

    return res;
  }

  initFromJson(jsonData: any) {
    jsonData.columns.forEach((column: any, index: any) => {
      this.columns[index].initFromJson(column);
    });
  }
}
