import PageContentElement from './PageContentElement';

export default class PageContentColumn {
  private pageElements: Array<PageContentElement> = [];

  get elements(): Array<PageContentElement> {
    return this.pageElements;
  }

  set elements(elements: Array<PageContentElement>) {
    this.pageElements = elements;
  }

  removeAt(index: number) {
    this.pageElements.splice(index, 1);
  }

  toJson() {
    const res: any = {
      elements: [],
    };

    this.elements.forEach((element: PageContentElement) => {
      res.elements.push(element.toJson());
    });

    return res;
  }


  initFromJson(jsonData: any) {
    jsonData.elements.forEach((element: any, index: any) => {
      this.elements.push(new PageContentElement(element.type));
      this.elements[index].initFromJson(element);
    });
  }
}
