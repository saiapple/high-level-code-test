import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    initTime: new Date(),
    sectionIndex: -1,
    rowIndex: -1,
    columnIndex: -1,
    elementIndex: -1,
    elementEdit: false,
    rowRender: new Date(),
    dragElement: '',
    dragging: false,
    dragColumnCount: 1,
    dragRows: false,
    rowDrop: false,
    drop: {
      isDrop: false,
      sectionIndex: -1,
      rowIndex: -1,
      columnIndex: -1,
      elementIndex: -1,
    },
  },
  mutations: {
    reInit(state) {
      state.initTime = new Date();
    },
    updateRowIndex(state, { index }) {
      state.rowIndex = index;
    },
    updateColumnIndex(state, { index }) {
      state.columnIndex = index;
    },
    updateElementIndex(state, { index }) {
      state.elementIndex = index;
    },
    updateElementEdit(state, { status }) {
      state.elementEdit = status;
    },
    updateDragElement(state, { elementType }) {
      state.dragElement = elementType;
    },
    updateDragStatus(state, { status }) {
      console.log('update state', status);
      state.dragging = status;
    },
    updateDropStatus(state, { isDrop, sectionIndex, rowIndex, columnIndex, elementIndex }) {
      state.drop.isDrop = isDrop;
      state.drop.sectionIndex = sectionIndex;
      state.drop.rowIndex = rowIndex;
      state.drop.columnIndex = columnIndex;
      state.drop.elementIndex = elementIndex;
    },
    reRenderRow(state) {
      state.rowRender = new Date();
    },
    updateDragRow(state, { status }) {
      state.dragRows = status;
    },
    updateDragColumnCount(state, { columns }) {
      state.dragColumnCount = columns;
    },
    updateRowDrop(state, { status }) {
      state.rowDrop = status;
    },
    updateSectionIndex(state, { index }) {
      state.sectionIndex = index;
    },
  },
  actions: {
  },
  modules: {
  },
});
