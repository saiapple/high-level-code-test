import Vue from 'vue';
import App from './App.vue';
import './assets/css/report.min.css';
import './assets/css/styles.min.scss';
import './assets/css/widgets.min.css';
import store from './store';

Vue.config.productionTip = false;

new Vue({
  store,
  render: (h) => h(App),
}).$mount('#app');
