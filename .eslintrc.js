module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
    '@vue/typescript/recommended',
  ],
  parserOptions: {
    ecmaVersion: 2020,
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'max-len': 0,
    'class-methods-use-this': 0,
    'no-undef': 0,
    'func-names': 0,
    'object-curly-newline': 0,
    'arrow-parens': 0,
    'space-before-function-paren': 0,
    'operator-linebreak': ['error', 'after'],
    'no-param-reassign': 0,
    '@typescript-eslint/no-explicit-any': 0,
    'import/no-unresolved': 0,
  },
};
